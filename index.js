'use strict';

/**
 * Transform promise into object { data, err }
 * @param {promise} promise
 * @return {object}
 */
function to(promise) {
  return promise.then(data => {
     return {
      data: data
    };
  })
  .catch(err => {
    return {
      err: err
    };
  });
};

module.exports = to;
